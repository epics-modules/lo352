#!../../bin/linux-x86_64/LOD

## You may have to change LOD to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"
epicsEnvSet("STREAM_PROTOCOL_PATH","$(TOP)/db")

## Register all support components
dbLoadDatabase("dbd/LOD.dbd",0,0)
LOD_registerRecordDeviceDriver(pdbbase) 

#Setup Asyn
drvAsynIPPortConfigure("LOD","172.16.8.2:59855 UDP*",0,0,0) 
asynOctetSetInputEos("LOD", -1, "\n") 
asynOctetSetOutputEos("LOD", -1, "\n") 
#asynSetTraceIOMask("LOD",-1,0x2) 
asynSetTraceIOMask("LOD",0,0x6)
#asynSetTraceMask("LOD",-1,0x9)
asynSetTraceMask("LOD",0,4)

## Load record instances
dbLoadRecords("db/LOD.db","PORT=LOD,A=0,LOD_N=01")

cd "${TOP}/iocBoot/${IOC}"
iocInit()
