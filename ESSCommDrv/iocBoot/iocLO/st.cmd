#!../../bin/linux-x86_64/LO

## You may have to change LO to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"
epicsEnvSet("STREAM_PROTOCOL_PATH","$(TOP)/db")

## Register all support components
dbLoadDatabase("dbd/LO.dbd",0,0)
LO_registerRecordDeviceDriver(pdbbase) 

#Setup Asyn
drvAsynIPPortConfigure("LO","172.16.8.4:59855 UDP*",0,0,0) 
asynOctetSetInputEos("LO", -1, "\n") 
asynOctetSetOutputEos("LO", -1, "\n") 
#asynSetTraceIOMask("LO",-1,0x2) 
#asynSetTraceIOMask("LO",0,0x6)
#asynSetTraceMask("LO",-1,0x9)
#asynSetTraceMask("LO",0,4)

## Load record instances
dbLoadRecords("db/LO.db","PORT=LO,A=0,LO_N=01")

cd "${TOP}/iocBoot/${IOC}"
iocInit()
